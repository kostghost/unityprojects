﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class controls : MonoBehaviour
{
    private readonly float Speed = 75.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKey(KeyCode.W))
	    {
	        transform.position += Time.deltaTime * Speed * transform.forward;
	    }

	    if (Input.GetKey(KeyCode.S))
	    {
	        transform.position -= Time.deltaTime * Speed * transform.forward;
	    }

	    if (Input.GetKey(KeyCode.A))
	    {
	        transform.position += Time.deltaTime * Speed * Vector3.Cross(transform.forward, transform.up);
	    }

	    if (Input.GetKey(KeyCode.D))
	    {
	        transform.position -= Time.deltaTime * Speed * Vector3.Cross(transform.forward, transform.up);
        }

	    Input.mousePosition = Vector3(0.5f, 0.5f, 0.0f);
	}
}
