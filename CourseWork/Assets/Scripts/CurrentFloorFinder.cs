﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentFloorFinder : MonoBehaviour
{
    public float FoundedFrictionCoef;

    void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log(other.gameObject.GetComponent<FloorSegmentScript>().FrictionCoefficient);
        FoundedFrictionCoef = other.gameObject.GetComponent<FloorSegmentScript>().FrictionCoefficient;
    }


}
