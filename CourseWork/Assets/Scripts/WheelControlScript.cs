﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelControlScript : MonoBehaviour
{
    public float MaxEngineTorque = 200f;
    public float MaxBreakTorque = 1000f;

    private WheelScript wheel;
	
    // Use this for initialization
	void Start ()
	{
	    wheel = GetComponent<WheelScript>();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    var inputRight = Input.GetKey(KeyCode.RightArrow);
        var inputLeft = Input.GetKey(KeyCode.LeftArrow);
        var inputSpace = Input.GetKey(KeyCode.Space);

        if (inputRight)
	        wheel.Torque = -MaxEngineTorque;
        else if (inputLeft)
	        wheel.Torque = MaxEngineTorque;
	    else if (inputSpace)
	    {
            if (wheel.Speed.x > 0.0)
                wheel.Torque = MaxBreakTorque;
            else if (wheel.Speed.x < 0.0f)
                wheel.Torque = -MaxBreakTorque;
        }
	    else
	    {
	        wheel.Torque = 0;
	    }
	}
}
