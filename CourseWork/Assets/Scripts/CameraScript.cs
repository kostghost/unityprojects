﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public GameObject Wheel;
    private Vector3 WheelPosition;

    public Texture2D texture;

	// Use this for initialization
	void Start ()
	{
	}
	
	// Update is called once per frame
	void Update ()
	{
	    WheelPosition = Wheel.transform.position;
		transform.position = new Vector3(WheelPosition.x, transform.position.y, transform.position.z);
	}
}
