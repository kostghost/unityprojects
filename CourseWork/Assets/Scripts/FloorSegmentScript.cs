﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorSegmentScript : MonoBehaviour
{

    public float FrictionCoefficient;

    //Вычисляет цвет по данному  friction coeff (нужно придумать цветовую схему)
    private Color CaclucateColor(float coef)
    {
        return new Color(1 - coef, 1 - coef, 1 - coef);
    }

    // Use this for initialization
    void Start ()
    {
        var spriteRenderer =  GetComponent<SpriteRenderer>();
        spriteRenderer.color = CaclucateColor(FrictionCoefficient);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
