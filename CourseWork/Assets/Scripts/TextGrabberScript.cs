﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


//Очень неуниверсальный класс
public class TextGrabberScript : MonoBehaviour
{
    public Transform ObjectToGrab;

    private WheelScript wheelScript;
    private Text textComponent;

	// Use this for initialization
	void Start ()
	{
	    textComponent = GetComponent<Text>();
	    wheelScript = ObjectToGrab.GetComponent<WheelScript>();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    textComponent.text = string.Format("Velocity: \n {0:0.##} м/c \n {1:0.##} км/ч \n \nCoord: \n {2:0.##} м", 
            wheelScript.Speed.x,
            wheelScript.Speed.x * 3.6,
            wheelScript.Position.x);

        //textComponent.text = "Velocity:" + wheelScript.Speed.x + " м/с = " + wheelScript.Speed.x * 3.6 + "км/ч  \n \n" +
        //                     "Coord:" + wheelScript.Position.x + " м";
	    
    }
}
