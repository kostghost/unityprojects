﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting;
using UnityEngine;
using Random = UnityEngine.Random;

struct RoadSegmentInfo
{
    public float Position; //Позиция начала участка по оси X
    public float Length;   //Длина участка в метрах
    public float FrictionCoefficent; //Коэффицент трения
};

public class RoadConstructorScript : MonoBehaviour
{
    public Transform RoadPrefab;
    public float YPosition = 0.0f;
    public float Height = 2f;

    private List<RoadSegmentInfo> roadInfo;


    //Загружаем дорогу (откуда-то, а пока просто генерируем)
    private List<RoadSegmentInfo>  LoadRoadInfo()
    {
        var road = new List<RoadSegmentInfo>();
        const int segmentCount = 100;

        //На время, для примера рандомно сгенерируем дорогу
        var lastSegment = new RoadSegmentInfo();
        for (int i = 0; i < segmentCount; i++)
        { 
            var newSegment = new RoadSegmentInfo()
            {
                Position = lastSegment.Position + lastSegment.Length,
                Length = Random.Range(0.5f, 5f),
                FrictionCoefficent = Random.Range(0.0f, 1.0f)
            };
            road.Add(newSegment);
            lastSegment = newSegment;
        }

        return road;
    }

    //public Transform GenerateRoadObject(RoadSegmentInfo segment)
    //{
    //    Transform obj;
    //    obj.position = new Vector3(segment.Position, 0); 
    //}

	// Use this for initialization
	void Start ()
	{
	    roadInfo = LoadRoadInfo();

        //Для теста сразу нарисуем эту дорогу
	    foreach (var el in roadInfo)
	    {
	        var obj = Instantiate(RoadPrefab);
            obj.position = new Vector3(el.Position, YPosition);
            obj.GetComponent<SpriteRenderer>().size = new Vector2(el.Length, Height);  //TODO Magic 2
	        obj.GetComponent<FloorSegmentScript>().FrictionCoefficient = el.FrictionCoefficent;

            obj.GetComponent<BoxCollider2D>().size = new Vector2(el.Length, Height*2);
            obj.GetComponent<BoxCollider2D>().offset = new Vector2(el.Length/2,Height/4);
	    }
	    
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
