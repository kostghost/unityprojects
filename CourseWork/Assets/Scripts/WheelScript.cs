﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelScript : MonoBehaviour
{
    //Все измерения тут - в м/час
    //1 юнит - 1 метр :)
    //Масса в КГ
    //положиьельное направление вращения - против часовой стрелки
    private const float G = 9.81f;


    public Vector2 Position;
    public Vector2 Speed;
    public Vector2 Acceleration;
    public float Angle;
    public float AngularSpeed;
    public float AngularAcceleration;

    public float WheelMass = 20;
    public float TotalMass = 250;
    public float Radius = 0.293f; //в метрах
           
    public float Torque; //Вращательный момент

    public float RollingResistanceCoefficient = 0.019f; // Коэффицент трения качения ( 0.019 - Асфальтобетонное в удовлетворительном состоянии)
    private float currentFrictionCoefficent;

    private float rollingResistanceTorque; //Момент силы трения качения

    private bool isSlippage = false; //Есть ли проскальзование


    private CurrentFloorFinder floorFinder;

    void Start ()
	{
        //TODO
	    currentFrictionCoefficent = 1;
	    floorFinder = GetComponent<CurrentFloorFinder>();
	}
	

	void FixedUpdate ()
	{
        //TODO
	    if (floorFinder) //если нашли дорогу
	    {
	        currentFrictionCoefficent = floorFinder.FoundedFrictionCoef;

	        Debug.Log(currentFrictionCoefficent * TotalMass * (Mathf.Pow(Radius, 2) + Mathf.Pow(Radius, 2)) / Radius);
            //проверка на проскальзование (Упрощенно, Вероятно, формула не верна)
            if (Mathf.Abs(Torque) > currentFrictionCoefficent * TotalMass * (Mathf.Pow(Radius, 2) + Mathf.Pow(Radius, 2)) / Radius)
            {
                //isSlippage = true;
            }
	    }


	   

        //Где-то тут нужно использовать дельта-тайминг

        //самый простой случай - без проскальзывания
        if (!isSlippage)
	    {
	        rollingResistanceTorque = TotalMass * G * RollingResistanceCoefficient;
	        if (AngularSpeed > 0) rollingResistanceTorque = -rollingResistanceTorque;

	        //Остановка при маленькой скорости
	        if (Mathf.Abs(AngularSpeed) < 0.1f  && Torque == 0.0f)
	        {
	            Torque = 0;
	            rollingResistanceTorque = 0;
	            AngularSpeed = 0;
	        }

            AngularAcceleration = (Torque + rollingResistanceTorque) / (TotalMass * Mathf.Pow(Radius, 2));
	        AngularSpeed += Time.deltaTime * AngularAcceleration;


	        Speed.x = -AngularSpeed * Radius;
	        Position += Speed * Time.deltaTime;
	        Angle += AngularSpeed * Time.deltaTime;

	    }
	    //Сам апдейт в пространстве



        transform.position = Position;

	    
	    transform.eulerAngles = new Vector3(0, 0, Angle * 180 / Mathf.PI);

	}
}
