﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponScript : MonoBehaviour
{

    public Transform shotPrefab; //префаб снаряда
    public float shootingRate = 0.25f;

    private float shootCooldown;


    //
    // Перезарядка
    //

	void Start ()
	{
	    shootCooldown = 0.0f;
	}
	
	void Update () {
	    if (shootCooldown > 0)
	    {
	        shootCooldown -= Time.deltaTime;
	    }
	}

    //
    // Стрельба из другого скрипта
    //

    //создаем снаряд, если возможно
    public void Attack(bool isEnemy)
    {
        if (CanAttack)
        {
            shootCooldown = shootingRate;

            //создадим новый выстрел
            var shotTransform = Instantiate(shotPrefab) as Transform;

            shotTransform.position = transform.position;

            ShotScript shot = shotTransform.gameObject.GetComponent<ShotScript>();
            if (shot != null)
            {
                shot.isEnemyShot = isEnemy;
            }

            //Направляем
            MoveScript move = shotTransform.gameObject.GetComponent<MoveScript>();
            if (move != null)
            {
                move.direction = this.transform.right; //Справа от спрайта
            }
        }
    }

    //Готово ли оружие выпустить снаряд?
    public bool CanAttack
    {
        get { return shootCooldown <= 0f; }
    }


}
